﻿using System.Web.Mvc;
using AuthenticationServer5.Filters;


// sets the filter for the MVC section

namespace AuthenticationServer5 {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new HttpsFilterMVC());

            //  filters.Add(new LoginFilter()); // disabled and set on each controller
        }
    }
}
