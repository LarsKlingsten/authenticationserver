﻿using System.Web.Http;

namespace AuthenticationServer5 {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{action}/{password}"
            // , defaults: new { password = RouteParameter.Optional }
            );

            //config.Routes.MapHttpRoute(
            //   name: "Directive",
            //   routeTemplate: "api/{controller}/{action}/{password}/{serviceId}/{clientName}"

            //);

            config.Routes.MapHttpRoute(
                 name: "RequestApi2",
                 routeTemplate: "api/{controller}/{action}/{password}/{serviceId}",
                 defaults: null,
                 constraints: new { serviceId = @"^[0-9]+$" }
             );



            config.Routes.MapHttpRoute(
               name: "RequestApi",
               routeTemplate: "api/{controller}/{action}/{password}/{serviceName}/{intentifier}",
               defaults: null,
               constraints: new { intentifier = @"^[a-z0-9]+$", serviceId = @"^[0-9]+$" }
            );
            
        }
    }
}