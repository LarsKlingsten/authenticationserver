﻿using System;
using System.Diagnostics;
using System.Data.Entity;
using System.Web.Mvc;
using DirectiveClassLibrary.EF;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using AuthenticationServer5.Filters;

namespace AuthenticationServer5.Controllers {

    [LoginFilter]
    public class AdminDirectiveController : Controller {

        private IDao daoService = DaoService.GetInstance();
        private DirectiveDBEntities db = new DirectiveDBEntities(); //  DaoService.GetInstance().GetDirectiveDBEntities();
        private ServiceLog log = ServiceLog.GetInstance();
        private Stopwatch sw = new Stopwatch();

        public ActionResult Index() {
            return RedirectToAction("Edit");
        }

        public ActionResult Edit() {
            VmDirective vmDirective = daoService.GetDirective();

            // panic 
            if (vmDirective == null) {
                log.AddLog(Status.Panic, $"No Directive Exist. Now Creating one", sw);

                try {
                    Directive directive = new Directive { ReloadSeconds = 60, UseDirective = true };
                    db.Directives.Add(directive);
                    db.SaveChanges();
                    log.AddLog(Status.Created, $"UseDirectives={vmDirective.UseDirective} reload={vmDirective.ReloadSeconds}", sw);
                    daoService.Reset();
                    vmDirective = daoService.GetDirective();
                }
                catch (Exception e) {
                    ModelState.AddModelError("IsEnabled", $"Panic: {e.Message}");
                    log.AddLog(Status.Panic, $"Directives UseDirectives={vmDirective.UseDirective} reload={vmDirective.ReloadSeconds} msg={e.Message}", sw);
                }
            }
            return View(vmDirective);
        }

        [HttpPost]
        public ActionResult Edit(VmDirective vmDirective) {
            try {
                Directive directive = db.Directives.Find(vmDirective.Id);
                directive.ReloadSeconds = vmDirective.ReloadSeconds;
                directive.UseDirective = vmDirective.UseDirective;
                directive.Updated = MyStatic.Now();

                try {
                    db.Entry(directive).State = EntityState.Modified;
                    db.SaveChanges();
                    log.AddLog(Status.Updated, $"Directives UseDirectives={vmDirective.UseDirective} reload={vmDirective.ReloadSeconds}", sw);
                    daoService.Reset();

                }
                catch (Exception e) {
                    ModelState.AddModelError("UseDirective", $"Panic: {e.Message}");
                    log.AddLog(Status.Panic, $"Directives UseDirectives={vmDirective.UseDirective} reload={vmDirective.ReloadSeconds} msg={e.Message}", sw);

                }
                return Redirect("/home/Index");
            }
            catch {
                return View();
            }
        }
    }
}