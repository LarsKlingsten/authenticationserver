﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Data.Entity;
using DirectiveClassLibrary.EF;
using Klingsten.DirectiveClassLibrary.Services;
using Klingsten.DirectiveClassLibrary.Models;
using AuthenticationServer5.Filters;

namespace AuthenticationServer5.Controllers {

    [LoginFilter]
    public class AdminUsersController : Controller {

        private IDao daoService = DaoService.GetInstance();
        private DirectiveDBEntities db = DaoService.GetInstance().GetDirectiveDBEntities();

        public ActionResult Index() {
            return View(daoService.GetAdminUsers());
        }

        public ActionResult Create() {
            return View(new VmAdminUser { IsEnabled = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Password, IsEnabled,IsAdmin")] VmAdminUser vmAdminUser) {


            if (ModelState.IsValid) {
                AdminUser adminUser = vmAdminUser.ToAdminUser();
                adminUser.Updated = MyStatic.Now();
                adminUser.Created = MyStatic.Now();

                try {
                    db.AdminUsers.Add(adminUser);
                    db.SaveChanges();
                    return RedirectToAction("Index"); // ok - all fine

                }
                catch (Exception e) {
                    ModelState.AddModelError("Name", "This Name was already taken.");

                }
            }
            return View(vmAdminUser);
        }

        public ActionResult Edit(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var adminUser = db.AdminUsers.Find(id);
            if (adminUser == null) {
                return HttpNotFound();
            }

            return View(VmAdminUser.ToVmAdminUser(adminUser));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Name,Password,IsEnabled,IsAdmin")] VmAdminUser vmAdminUser) {

            AdminUser adminUser;
            if (ModelState.IsValid) {
                adminUser = db.AdminUsers.Find(vmAdminUser.Id);
                adminUser.Name = vmAdminUser.Name;
                adminUser.Password = vmAdminUser.Password;
                adminUser.IsEnabled = vmAdminUser.IsEnabled;
                adminUser.IsAdmin = vmAdminUser.IsAdmin;
                adminUser.Updated = MyStatic.Now();

                try {
                    db.Entry(adminUser).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception e) {
                    ModelState.AddModelError("Name", "This Name was already taken. Please try another one. ");
                    return View(vmAdminUser);
                }

                return RedirectToAction("Index");
            }
            adminUser = db.AdminUsers.Find(vmAdminUser.Id);
            return View(VmAdminUser.ToVmAdminUser(adminUser));
        }

        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdminUser adminUser = db.AdminUsers.Find(id);
            if (adminUser == null) {
                return HttpNotFound();
            }
            return View(VmAdminUser.ToVmAdminUser(adminUser));
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            AdminUser adminUser = db.AdminUsers.Find(id);
            db.AdminUsers.Remove(adminUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}