﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DirectiveClassLibrary.EF;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using System.Diagnostics;
using System.Collections.Generic;
using AuthenticationServer5.Filters;

namespace AuthenticationServer5.Controllers {

    [LoginFilter]
    public class AllowIpAddressController : Controller {

        private DirectiveDBEntities db = new DirectiveDBEntities();
        private IDao daoService = DaoService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();
        private Stopwatch sw = new Stopwatch();

        public ActionResult Index() {
            List<VmAllowIpAddress> ipAddresss = daoService.GetAllowedIpAddresses();
            return View(ipAddresss);
        }

        public ActionResult Edit(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmAllowIpAddress vmAllowedIpAddress = daoService.GetAllowedIpAddresses().Where(s => s.Id == (int)id).FirstOrDefault(); ;
            if (vmAllowedIpAddress == null) {
                return HttpNotFound();
            }
            return View(vmAllowedIpAddress);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VmAllowIpAddress vmAllowedIP) {
            sw.Restart();
            if (ModelState.IsValid) {
                AllowIpAddress allowedIpAddress = db.AllowIpAddresses.Find(vmAllowedIP.Id);
                allowedIpAddress.IsEnabled = vmAllowedIP.IsEnabled;
                allowedIpAddress.IpAddress = vmAllowedIP.IpAddress;
                allowedIpAddress.Comment = vmAllowedIP.Comment;
                allowedIpAddress.Updated = MyStatic.Now();

                try {
                    db.Entry(allowedIpAddress).State = EntityState.Modified;
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Updated, $"Ipaddresses IP={vmAllowedIP.IpAddress}", sw);
                    return RedirectToAction("Index");
                }
                catch (Exception e) {
                    ModelState.AddModelError("IpAddress", "Ups: " + e.Message);
                }
            }
            return View(vmAllowedIP);
        }

        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmAllowIpAddress allowIpAddress = daoService.GetAllowedIpAddresses().Where(s => s.Id == (int)id).FirstOrDefault(); ;
            if (allowIpAddress == null) {
                return HttpNotFound();
            }
            return View(allowIpAddress);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            AllowIpAddress user = db.AllowIpAddresses.Find(id);
            db.AllowIpAddresses.Remove(user);
            db.SaveChanges();
            daoService.Reset();
            log.AddLog(Status.Deleted, $"AllowedIpAdddress ip= {user.IpAddress}", sw);
            return RedirectToAction("Index");
        }

        public ActionResult Create() {
            return View(new VmAllowIpAddress { IsEnabled = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VmAllowIpAddress vmAllowedIpAddress) {
            if (ModelState.IsValid) {

                AllowIpAddress allowedIpAddress = new AllowIpAddress {
                    IpAddress = vmAllowedIpAddress.IpAddress,
                    Comment = vmAllowedIpAddress.Comment,
                    Created = MyStatic.Now(),
                    Updated = MyStatic.Now(),
                    IsEnabled = vmAllowedIpAddress.IsEnabled
                };

                try {
                    db.AllowIpAddresses.Add(allowedIpAddress);
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Created, $"IpAddress={vmAllowedIpAddress.IpAddress}", sw);
                    return RedirectToAction("Index");
                }
                catch (Exception e) {
                    ModelState.AddModelError("IpAddress", "Error: " + e.Message);
                }
            }
            return View(vmAllowedIpAddress);
        }
    }
}