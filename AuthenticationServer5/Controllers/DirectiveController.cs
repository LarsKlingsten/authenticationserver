﻿using System.Web.Http;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using AuthenticationServer5.Filters;

namespace AuthenticationServer5.Controllers {

    [LoginFilter]
    public class DirectiveController : ApiController {

        private IDao daoService = DaoService.GetInstance();
        private OnlineClientService onlineClientService = OnlineClientService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();
        private Stopwatch sw = new Stopwatch();

        [HttpGet]
        [Route("api/Directive/{pwd}/{serviceName}/{clientID}")]
        public ApiResponse UseDirective(string pwd, string serviceName, string clientID) {
            if (daoService.AuthenticateServicePassword(pwd) == false) {
                return MyStatic.PASSWORD_REJECTED;
            }

            sw.Restart();
            bool useDirective = daoService.GetDirective().UseDirective;

            bool IsDirectiveToBeUpdated = false;
            VmDirective vmDirective = daoService.GetDirective().CopyOfDirective();
            OnlineClient onlineClient;

            // combine a request for a single servive and a request for all services into a single iteratable list
            IList<VmService> vmServices;
            if (serviceName.Equals("All")) {
                vmServices = daoService.GetServices();
            } else {
                VmService vmService = daoService.GetServiceByName(serviceName);
                if (vmService == null) {
                    return MyStatic.SERVICES_DOES_NOT_EXIST;
                }
                vmServices = new List<VmService>();
                vmServices.Add(vmService);
            }
             
            foreach (VmService vmService in vmServices) {
                onlineClient = onlineClientService.GetOnlineClient(clientID, vmService.id);
                onlineClientService.UpdateOnlineClientAccessTime(onlineClient, vmService);

                int accessFactor = 100;
                if (useDirective) {
                    accessFactor = onlineClientService.CalcAccessFactor(vmService.id);
                }

                vmDirective.Services.Add(UpdateUserAccessLevels(vmService, accessFactor));

                if (onlineClient.ClientMustUpdateDirectiveNow) {
                    IsDirectiveToBeUpdated = true;
                    onlineClient.ClientMustUpdateDirectiveNow = false;
                }
            }

            log.AddLog(Status.Info, $"ServiceName={serviceName} clientID={clientID} IsDirectiveToBeUpdated={IsDirectiveToBeUpdated}", sw);

            if (IsDirectiveToBeUpdated) {
                return new ApiResponse(HttpCode.OK, "Directive", vmDirective.ToJObjectIgnoreNull());
            } else {
                return new ApiResponse(HttpCode.Not_Modified, "Not Modified (You've latest version)", null);
            }
        }

        [HttpGet]
        [Route("api/Directive/Reset/{password}")]
        public ApiResponse Reset(string password) {
            if (daoService.AuthenticateServicePassword(password) == false) {
                return MyStatic.PASSWORD_REJECTED;
            }
            daoService.Reset();
            return new ApiResponse(HttpCode.Reset_Content, "Reset successful", null);
        }

        private VmService UpdateUserAccessLevels(VmService vmService, int accessfactor) {

            VmService tempVmService = new VmService {
                name = vmService.name,
                id = vmService.id,
                apikey = vmService.apikey,
                TimeOutMilliSec = vmService.TimeOutMilliSec,
                cacheSeconds = vmService.cacheSeconds
            };

            foreach (VmEndPoint vmUser in vmService.endPoints.Where(s => s.IsEnabled == true)) {
                tempVmService.addEndPoint(new VmEndPoint {
                    Deployment = vmUser.Deployment,
                    Url = vmUser.Url
                });
            }

            foreach (VmUser vmUser in vmService.users.Where(s => s.Enabled == true)) {
                foreach (VmUserService vmUserService in vmUser.usersServices.Where(s => s.ServiceID == vmService.id))
                    tempVmService.addUserService(new VmUserService {
                        Limit = vmUserService.Limit * accessfactor / 100,
                        Reset = vmUserService.Reset,
                        ApiKey = vmUserService.ApiKey,
                        ServiceID = vmUserService.ServiceID,
                        httpGet = vmUserService.httpGet,
                        httpPost = vmUserService.httpPost,
                        httpPut = vmUserService.httpPut,
                    });
            }

            tempVmService.users = null;

            return tempVmService;
        }
    }
}