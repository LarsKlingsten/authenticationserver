﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using AuthenticationServer5.Filters;

namespace AuthenticationServer5.Controllers {

    [LoginFilter]
    public class HumanLogController : Controller {

        private IDao daoService = DaoService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();

        public ActionResult Index() {
            List<LogEntry> logs = log.GetLogs().OrderByDescending(s => s.Updated).ToList(); ;
            return View(logs);
        }
    }
}
