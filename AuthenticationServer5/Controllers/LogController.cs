﻿using System.Linq;
using System.Web.Http;
using System.Collections.Generic;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;

namespace AuthenticationServer5.Controllers {

 
    public class LogController : ApiController {

        private IDao daoService = DaoService.GetInstance();
        private OnlineClientService onlineClientService = OnlineClientService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();

        [HttpGet]
        [Route("api/Log/json/{pwd}")]
        public ApiResponse Json(string pwd) { // // Get the logs as Json
            if (!pwd.Equals(MyStatic.PASSWORD_FOR_SERVICE)) {
                return MyStatic.API_SERVICE_PASSWORD_REJECTED;
            }
            return new ApiResponse(HttpCode.OK, "LogService", new { log = log.GetLogs() }.ToJObjectIgnoreNull());
        }

        [HttpGet]
        [Route("api/log/human/{pwd}")]
        public ApiResponse Human(string pwd) { // Get the logs as human readible

            if (!pwd.Equals(MyStatic.PASSWORD_FOR_SERVICE)) {
                return MyStatic.API_SERVICE_PASSWORD_REJECTED;
            }
            return new ApiResponse(HttpCode.OK, "VmDirective", new { log = log.GetLogsAsString() }.ToJObjectIgnoreNull());
        }

        [HttpGet]
        [Route("api/log/ListServices/{password}")]
        public ApiResponse ListServices(string password) { // list the services

            if (daoService.AuthenticateServicePassword(password) == false) {
                return MyStatic.PASSWORD_REJECTED;
            }

            List<VmService> tempVmServices = new List<VmService>();
            foreach (VmService s in daoService.GetServices()) {
                tempVmServices.Add(new VmService { name = s.name, id = s.id, apikey = s.apikey, cacheSeconds = s.cacheSeconds });
            }
            return new ApiResponse(HttpCode.OK, "VmDirective", new VmDirective { Services = tempVmServices }.ToJObjectIgnoreNull());
        }

        [HttpGet]
        [Route("api/log/ListClients/{password}")]
        public ApiResponse ListClients(string password) { // list the clients that has previuosly connected.

            if (daoService.AuthenticateServicePassword(password) == false) {
                return MyStatic.PASSWORD_REJECTED;
            }
            List<OnlineClient> clients = onlineClientService.GetAllOnlineClients().OrderBy(s => s.ServiceId).ToList();
            return new ApiResponse(HttpCode.OK, "OnlineClient", new { OnlineClients = clients }.ToJObjectIgnoreNull());
        }
    }
}