﻿using System.Web.Mvc;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using DirectiveClassLibrary.EF;
using System.Linq;

namespace AuthenticationServer5.Controllers {
 
    public class LoginController : Controller {
        private DirectiveDBEntities db = new DirectiveDBEntities(); //  DaoService.GetInstance().GetDirectiveDBEntities();

        public ActionResult Index() {
            return View(new VmAdminUser());
        }

        /// summary
        ///          If login fails - reset the session state, and display error message  
        ///          if login succeeds - save the session state and redirect to home page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(VmAdminUser vmAdminUser) {
            if (ModelState.IsValid) {

                IDao daoService = DaoService.GetInstance();

                AdminUser adminUser = db.AdminUsers
                    .Where(s => s.Name.Equals(vmAdminUser.Name)
                    && s.Password.Equals(vmAdminUser.Password))
                    .FirstOrDefault();

                if (adminUser == null) {
                    Session[MyStatic.SessionUser] = null;
                    ModelState.AddModelError("Name", "Bad User/password combination");
                }
                else {
                    Session[MyStatic.SessionUser] = adminUser;
                    return Redirect("/home/index");
                }
            }
            // failed to login (try again)
            return View(new VmAdminUser());
        }
    }
}
