﻿using System.Web.Http;
using System.Diagnostics;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;

namespace AuthenticationServer5.Controllers {
    public class RequestController : ApiController {

        private UserStatService userStatService = UserStatService.GetInstance();
        private DaoService daoService = DaoService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();
        private Stopwatch sw = new Stopwatch();

        [HttpGet]
        [Route("api/Request/Auth/{pwd}/{serviceName}/{apiKey}")]
        public ApiResponse Auth(string pwd, string serviceName, string apiKey) {
            sw.Restart();

            if (daoService.AuthenticateServicePassword(pwd) == false) {
                return MyStatic.API_SERVICE_PASSWORD_REJECTED;
            }

            // ensure that the the request controller is only 
            // used when the Service in DirectiveMode
            // set directive here: http://authenticationserver5.azurewebsites.net/AdminDirective/Edit

            VmDirective vmDirective = daoService.GetDirective();
            if (vmDirective.UseDirective) {
                return MyStatic.USE_DIRECTIVE_INSTEAD_OF_REQUEST;
            }

            // try to get userStats from previous request
            string key = userStatService.GetKey(apiKey, serviceName);
            UserStat userStat = userStatService.GetUserStatByKey(key);   

            if (userStat == null) {

                // Test Service Name
                VmService vmService = daoService.GetServiceByName(serviceName);
                if (vmService == null) {
                    return MyStatic.SERVICES_DOES_NOT_EXIST;
                }

                // Ttest User's ApiKey
                VmUser vmUser = daoService.GetUser(apiKey);
                if (vmUser == null) {
                    return MyStatic.USER_APIKEY_DOES_NOT_EXIST;
                }

                // Test Usage Rights
                VmUserService vmUserService = daoService.GetVmUserService(vmUser, vmService);
                if (vmUserService == null) {
                    return MyStatic.USER_NO_ACCESS_TO_SERVICE;
                }
                userStat = userStatService.CreateUserStat(apiKey, serviceName, vmUserService, vmDirective); // User has authenticates and authorizes to the service -> create a new entry  
            }

            userStat.addCount();

            bool withinLimit = userStatService.IsWithinLimits(userStat);
            log.AddLog(Status.Info, $"Req serviceName={serviceName} apiKey={ apiKey} withinLimtits={withinLimit}", sw);

            if (!withinLimit) {

                // declined
                userStat.addCountDeclined();
                return new ApiResponse(HttpCode.Too_Many_requests, "RequestResponse", new RequestResponse {
                    IsAuthorized = false,
                    Message = "Too Many Requests. Number of requests exceeds limit.",
                    Reset = userStat.NextResetInSeconds(),
                    Limit = userStat.VmUserService.Limit
                }.ToJObjectIgnoreNull());
            }

            // Accepted/All is good          
            return new ApiResponse(HttpCode.OK, "RequestResponse", new RequestResponse {
                IsAuthorized = true,
                Reset = userStat.NextResetInSeconds(),
                Limit = userStat.VmUserService.Limit,
                Remaining = userStat.Remaining
            }.ToJObjectIgnoreNull());
        }
    }
}