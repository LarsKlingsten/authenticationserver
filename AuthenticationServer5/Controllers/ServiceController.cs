﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Data.Entity;
using DirectiveClassLibrary.EF;
using Klingsten.DirectiveClassLibrary.Services;
using Klingsten.DirectiveClassLibrary.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AuthenticationServer5.Filters;

namespace AuthenticationServer5.Controllers {

    [LoginFilter]
    public class ServiceController : Controller {

        private IDao daoService = DaoService.GetInstance();
        private DirectiveDBEntities db = new DirectiveDBEntities(); //  DaoService.GetInstance().GetDirectiveDBEntities();
        private ServiceLog log = ServiceLog.GetInstance();
        private Stopwatch sw = new Stopwatch();

        public ActionResult Index() {
            IEnumerable<VmService> services = daoService.GetServices().OrderBy(s => s.name);
            return View(services);
        }

        public ActionResult Create() {
            return View(new VmService {
                IsEnabled = true,
                Limit = 100,
                Reset = 60,
                cacheSeconds = 60,
                RequestFormat = "*",
                httpGet = true,
                TimeOutMilliSec = 5000
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VmService vmService) {

            if (ModelState.IsValid) {
                Service service = vmService.ToService();
                service.Updated = MyStatic.Now();
                service.Created = MyStatic.Now();

                try {
                    service.EndPoints.Add(NewEndPoint());
                    db.Services.Add(service);
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Created, $"Service={service.Name}", sw);
                    return RedirectToAction("Index"); // ok - all fine
                }
                catch (Exception e) {
                    ModelState.AddModelError("Name", "This Service name was already taken. Please try another one.");
                }
            }
            return View(vmService);
        }

        public ActionResult Edit(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmService vmService = daoService.GetService((int)id);
            if (vmService == null) {
                return HttpNotFound();
            }
            ViewBag.EndPoints = vmService.endPoints;
            return View(vmService);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VmService vmService) {
            sw.Restart();
            Service service = db.Services.Find(vmService.id);

            if (ModelState.IsValid) {
                service.Name = vmService.name;
                service.ApiKey = vmService.apikey;
                service.IsEnabled = vmService.IsEnabled;
                service.Reset = vmService.Reset;
                service.Limit = vmService.Limit;
                service.HttpGet = vmService.httpGet;
                service.HttpPost = vmService.httpPost;
                service.HttpPut = vmService.httpPut;
                service.HttpGet = vmService.httpGet;
                service.HttpGet = vmService.httpGet;
                service.TimeOutMilliSec = vmService.TimeOutMilliSec;
                service.RequestFormat = vmService.RequestFormat;
                service.CacheSeconds = vmService.cacheSeconds;
                service.Updated = MyStatic.Now();
                try {
                    db.Entry(service).State = EntityState.Modified;
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Updated, $"Service={service.Name}", sw);
                    return RedirectToAction("Index");
                }
                catch (Exception e) {
                    ModelState.AddModelError("Name", "This Service name was already taken. Please try another one.");
                }
            }
            ViewBag.EndPoints = daoService.GetService(vmService.id).endPoints;
            return View(vmService);
        }

        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmService service = daoService.GetService((int)id);
            if (service == null) {
                return HttpNotFound();
            }
            return View(service);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            sw.Restart();
            Service service = db.Services.Find(id);
            db.Services.Remove(service);
            db.SaveChanges();
            daoService.Reset();
            log.AddLog(Status.Warning, $"deleted Service={service.Name}", sw);
            return RedirectToAction("Index");
        }

        ///
        /// END POINTS ///
        ///

        public ActionResult CreateEndPoint(int? id) { // id = serviceID
            Service service = db.Services.Find(id);
            service.EndPoints.Add(NewEndPoint());
            db.Entry(service).State = EntityState.Modified;
            db.SaveChanges();
            daoService.Reset();
            log.AddLog(Status.Created, $"created EndPoint to Service={service.Name}", sw);
            return RedirectToAction("edit/" + id);
        }

        public ActionResult EditEndPoint(int? id) { // id = EndPointId
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var vmEndPoint = daoService.GetEndPoint((int)id);
            if (vmEndPoint == null) {
                return HttpNotFound();
            }
            return View(vmEndPoint);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEndPoint(VmEndPoint vmEndPoint) {
            sw.Restart();
            DirectiveClassLibrary.EF.EndPoint endpoint = db.EndPoints.Find(vmEndPoint.Id);

            if (ModelState.IsValid) {
                endpoint.Deployment = vmEndPoint.Deployment;
                endpoint.IsEnabled = vmEndPoint.IsEnabled;
                endpoint.Url = vmEndPoint.Url;
                endpoint.Updated = MyStatic.Now();
                try {
                    db.Entry(endpoint).State = EntityState.Modified;
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Updated, $"EndPointId={ vmEndPoint.Id} url={endpoint.Url}", sw);
                    return RedirectToAction("edit/" + vmEndPoint.ServiceId);
                }
                catch (Exception e) {
                    ModelState.AddModelError("Url", e.Message); // should not happen
                }
            }
            return View(vmEndPoint);
        }

        public ActionResult DeleteEndPoint(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmEndPoint vmEndPoint = daoService.GetEndPoint((int)id);
            if (vmEndPoint == null) {
                return HttpNotFound();
            }
            return View(vmEndPoint);
        }

        [HttpPost, ActionName("DeleteEndPoint")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteEndPointConfirmed(int id) {
            sw.Restart();
            DirectiveClassLibrary.EF.EndPoint EndPoint = db.EndPoints.Find(id);
            db.EndPoints.Remove(EndPoint);
            db.SaveChanges();
            daoService.Reset();
            log.AddLog(Status.Deleted, $"deleted EndPoint={EndPoint.Url}", sw);
            return RedirectToAction("edit/" + EndPoint.Fk_service);
        }

        private DirectiveClassLibrary.EF.EndPoint NewEndPoint() {
            return new DirectiveClassLibrary.EF.EndPoint {
                Url = "https://",
                Deployment = Deployment.Prod.ToString(),
                Created = MyStatic.Now(),
                Updated = MyStatic.Now()
            };
        }
    }
}