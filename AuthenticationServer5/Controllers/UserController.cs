﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DirectiveClassLibrary.EF;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using System.Diagnostics;
using System.Data.Entity.Validation;
using System.Collections.Generic;
using AuthenticationServer5.Filters;

namespace AuthenticationServer5.Controllers {

    [LoginFilter]
    public class UserController : Controller {

        private IDao daoService = DaoService.GetInstance();
        private DirectiveDBEntities db = new DirectiveDBEntities();
        private ServiceLog log = ServiceLog.GetInstance();
        private Stopwatch sw = new Stopwatch();

        public ActionResult Index() {
            return View(daoService.GetUsers().OrderBy(s => s.Name));
        }

        public ActionResult Create() {
            return View(new VmUser { Origons = "*" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VmUser vmUser) {
            if (ModelState.IsValid) {

                User user = vmUser.ToUser();
                user.Updated = MyStatic.Now();
                user.Created = MyStatic.Now();

                try {
                    db.Users.Add(user);
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Created, $"user name={vmUser.Name}", sw);
                    return RedirectToAction("Index");
                } catch (Exception e) {
                    ModelState.AddModelError("ApiKey", "This key has already taken. Please try another one.");
                }
            }
            return View(vmUser);
        }

        public ActionResult Edit(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmUser vmUser = daoService.GetUserById((int)id);
            if (vmUser == null) {
                return HttpNotFound();
            }
            ViewBag.UserServices = vmUser.usersServices;
            ViewBag.AddService = GetDropdownServices(new List<VmUserService>(vmUser.usersServices));
            return View(vmUser);
        }

        /// summary:
        ///           Get the services that user does not presently subscribe to.
        ///           Ensures that user can only subscribe to a service once.
        private List<SelectListItem> GetDropdownServices(List<VmUserService> vmUserServices) {
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (VmService service in daoService.GetServices()) {
                if (!vmUserServices.Exists(s => s.ServiceID == service.id)) {
                    items.Add(new SelectListItem {
                        Text = service.name,
                        Value = service.id.ToString()
                    });
                }
            }
            return items;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VmUser vmUser) {
            if (ModelState.IsValid) {
                User user = db.Users.Find(vmUser.Id);
                user.IsEnabled = vmUser.Enabled;
                user.ApiKey = vmUser.ApiKey;
                user.Company = vmUser.Company;
                user.Name = vmUser.Name;
                user.Origons = vmUser.Origons;
                user.Updated = MyStatic.Now();

                try {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Updated, $"user name={vmUser.Name}", sw);
                    return RedirectToAction("Index");
                } catch (DbEntityValidationException valEx) { // this should not be necessary (as each field is annotated)- however is precautionary if I have overlooked a possible error
                    foreach (var exception in valEx.EntityValidationErrors) {
                        foreach (var error in exception.ValidationErrors) {
                            ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                        }
                    }
                } catch (Exception e) {
                    ModelState.AddModelError("ApiKey", "This key already taken. Please try another one.");
                }
            }
            ViewBag.AddService = GetDropdownServices(new List<VmUserService>(vmUser.usersServices));
            return View(vmUser);
        }

        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmUser vmUser = daoService.GetUserById((int)id);
            if (vmUser == null) {
                return HttpNotFound();
            }
            return View(vmUser);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            daoService.Reset();
            log.AddLog(Status.Deleted, $"user name= {user.Name}", sw);
            return RedirectToAction("Index");
        }

        /// 
        /// ADD USERSERVICE - PARTIAL PAGES///
        ///  
        /// http://localhost:62139/User/AddService?Id=3&AddService=1

        [HttpGet]
        public ActionResult AddService(int? id, int? addService) { // id==UserID addservice==serviceID
            VmService vmService = daoService.GetService((int)addService);

            // adds a new service using standard formats
            UserService userService = new UserService {
                Fk_service = (int)addService,
                Fk_user = (int)id,
                HttpGet = vmService.httpGet,
                HttpPost = vmService.httpPost,
                HttpPut = vmService.httpPut,
                Limit = vmService.Limit,
                Reset = vmService.Reset,
                IsEnabled = true,
                Updated = MyStatic.Now(),
                Created = MyStatic.Now()
            };
            db.UserServices.Add(userService);
            db.SaveChanges();
            daoService.Reset();
            log.AddLog(Status.Created, $"userService UserID={userService.Fk_user} ServiceID={userService.Fk_service}", sw);
            return RedirectToAction("edit/" + id);
        }

        public ActionResult DeleteUserService(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmUserService vmUserService = daoService.GetUserService((int)id);
            if (vmUserService == null) {
                return HttpNotFound();
            }
            return View(vmUserService);
        }

        [HttpPost, ActionName("DeleteUserService")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUserServiceConfirmed(int id, int userID) {
            UserService userService = db.UserServices.Find(id);
            db.UserServices.Remove(userService);
            db.SaveChanges();
            daoService.Reset();
            log.AddLog(Status.Deleted, $"user name= {userService.Id}", sw);
            return RedirectToAction("edit/" + userID);
        }

        public ActionResult EditUserService(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmUserService vmUserService = daoService.GetUserService((int)id);
            if (vmUserService == null) {
                return HttpNotFound();
            }
            return View(vmUserService);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUserService(VmUserService vmUserService) {
            if (ModelState.IsValid) {
                UserService userService = db.UserServices.Find(vmUserService.Id);
                userService.HttpGet = vmUserService.httpGet;
                userService.HttpPost = vmUserService.httpPost;
                userService.HttpPut = vmUserService.httpPut;
                userService.Limit = vmUserService.Limit;
                userService.Reset = vmUserService.Reset;
                userService.Updated = MyStatic.Now();
                try {
                    db.Entry(userService).State = EntityState.Modified;
                    db.SaveChanges();
                    daoService.Reset();
                    log.AddLog(Status.Updated, $"user name={vmUserService.Id}", sw);
                    return RedirectToAction("edit/" + vmUserService.UserID);
                } catch (Exception e) {
                    ModelState.AddModelError("ApiKey", "This key already taken. Please try another one.");
                }
            }
            return View(vmUserService);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
