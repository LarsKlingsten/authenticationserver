﻿using System;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;

namespace AuthenticationServer5.Filters {

    /// summary:
    ///           Set Globally at filterconfig.cs (called from Global.asax.cs) for all API requests
    public class HttpsFilter : ActionFilterAttribute {

        public override void OnActionExecuting(HttpActionContext actionContext) {

            // enforce https on non-local requests
            if (!actionContext.Request.IsLocal()) {

                if (!string.Equals(actionContext.Request.RequestUri.Scheme, "https", StringComparison.OrdinalIgnoreCase)) {
                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest) {
                        Content = new StringContent("This Site Requires HTTPS")
                    };
                }
            }
            return;
        }
    }
}