﻿using System;
using System.Web;
using System.Web.Mvc;

namespace AuthenticationServer5.Filters {

    public class HttpsFilterMVC : ActionFilterAttribute {
        public override void OnActionExecuting(ActionExecutingContext context) {

            HttpRequestBase request = context.HttpContext.Request;
            HttpResponseBase response = context.HttpContext.Response;

            if (!request.IsLocal) {

                if (!request.IsSecureConnection) {
                    var builder = new UriBuilder(request.Url) {
                        Scheme = Uri.UriSchemeHttps,
                        Port = 443
                    };
                    string url = builder.Uri.ToString();
                    response.Redirect(url);
                }
            }
            base.OnActionExecuting(context);
        }
    }
}