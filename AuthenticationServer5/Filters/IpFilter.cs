﻿using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;

namespace AuthenticationServer5.Filters {

    /// summary:
    ///          This has been set at global.asex.cs file
    public class IpFilter : ActionFilterAttribute {

        private ServiceLog log = ServiceLog.GetInstance();
        private DaoService daoService = DaoService.GetInstance();

        public override void OnActionExecuting(HttpActionContext actionContext) {
            var context = actionContext.Request.Properties["MS_HttpContext"] as System.Web.HttpContextBase;
            string userIP = context.Request.UserHostAddress;

            if (!daoService.IsIpAddressAllowed(userIP)) {
                var BadIp = $"Request from {userIP} is Unauthorized";
                actionContext.Response =
                   new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden) {
                       Content = new StringContent(BadIp)
                   };
                log.AddLog(Status.Warning, BadIp);
                return;
            }
        }
    }
}