﻿using System.Web;
using System.Web.Mvc;
using Klingsten.DirectiveClassLibrary.Models;

namespace AuthenticationServer5.Filters {

    /// summary:
    ///           Checks the session attribute to see whether a user is 
    ///           logged in or not, if not, then redicts to the login page
    /// 
    ///           The loginFilter is used as an annotation [loginfilter] on
    ///           the controllers that requires the user to be logged in. 
    /// 
    ///           Alternative it can be set globally @ global.asex.cs file (which has been done here)
    public class LoginFilter : ActionFilterAttribute {

        public override void OnActionExecuting(ActionExecutingContext filterContext) {

            if (HttpContext.Current.Session[MyStatic.SessionUser] == null) {
                filterContext.Result = new RedirectResult("~/login");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}