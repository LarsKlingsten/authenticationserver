﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AuthenticationServer5.Filters;


namespace AuthenticationServer5 {
    public class WebApiApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterWebApiFilters(GlobalConfiguration.Configuration.Filters); // calls and sets Api Filters
        }

        /// summary
        ///          Add the Filters to the API
        public static void RegisterWebApiFilters(System.Web.Http.Filters.HttpFilterCollection filters) {
            filters.Add(new HttpsFilter());
            filters.Add(new IpFilter());
        }

    }
}
