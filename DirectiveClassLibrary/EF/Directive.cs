//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DirectiveClassLibrary.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Directive
    {
        public int Id { get; set; }
        public int ReloadSeconds { get; set; }
        public bool UseDirective { get; set; }
        public System.DateTime Updated { get; set; }
    }
}
