﻿using Newtonsoft.Json.Linq;
using System;

namespace Klingsten.DirectiveClassLibrary.Models {
    
    public class ApiResponse {
        public HttpCode StatusCode { get; set; }
        public string Message { get; set; }
        public JObject JSon { get; set; }

        public ApiResponse(HttpCode statusCode, string message, JObject jSon) {
            this.StatusCode = statusCode;
            this.Message = message;
            this.JSon = jSon;
        }

        public override string ToString() {
            return $"msg={Message} Status={StatusCode}  ";
        }
    }
}