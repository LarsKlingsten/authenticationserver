﻿namespace Klingsten.DirectiveClassLibrary.Models {

    public enum Status { Warning, Info, Panic, Deleted, Updated, Created, Unchanged };

    public enum HttpCode {
        OK = 200,
        No_Content = 204,
        Reset_Content = 205,
        Not_Modified = 304,
        Bad_Request = 400,
        Unauthorized = 401,
        Too_Many_requests = 429,
        Not_Found = 404,
        Internal_Server_Error = 500,
        Not_Implemented = 501
    }

    // see also http://authenticationserver5.azurewebsites.net/api/directive/ListServices/{pwd} 
    public enum MicroService {
        All,
        StoreProduct,
        NewsLetters,
        Vacancies,
        Locations,
        JobLocations,
        Hours,
        Events,
        StoreNumbers,
        JobAgents,
        OnlineProducts
    }

    public enum Deployment {
        Prod,
        Dev,
        Test,
        Other
    }
}