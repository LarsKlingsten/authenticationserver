﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace Klingsten.DirectiveClassLibrary.Models {
    public static class Extensions {

        public static string Left(this string str, int stringLength) {
            if (str == null || stringLength <= 0) { return ""; }
            if (stringLength > str.Length) { stringLength = str.Length; }
            return str.Substring(0, stringLength);
        }

        public static string TimeUtc(this string str) {
            return (string.Format("{0} {1}", System.DateTime.UtcNow.ToString("hh:mm:ss.fff"), str));
        }
        public static decimal TimerTickToMilliSeconds(this long ticks) {
            return decimal.Divide(ticks * 1000, Stopwatch.Frequency);
        }

        private static JsonSerializerSettings JSON_IGNORENULL = new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore };
        public static JObject ToJObjectIgnoreNull(this object o) {
            return JObject.Parse(JsonConvert.SerializeObject(o, JSON_IGNORENULL));
        }
        public static JObject ToJObject(this object o) {
            return JObject.Parse(JsonConvert.SerializeObject(o));
        }
    }
}