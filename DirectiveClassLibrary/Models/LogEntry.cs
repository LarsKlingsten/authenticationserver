﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class LogEntry {

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }
        public string Text { get; set; }
        public Status Status { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:0.000}")]
        public decimal millisec { get; set; }

        public LogEntry(Status status, string text, Stopwatch sw) {
            Status = status;
            Text = text;
            Updated = MyStatic.Now();

            if (sw != null) {
                millisec = sw.ElapsedTicks.TimerTickToMilliSeconds();
            }
            else {
                millisec = -1m;
            }
        }

        public override string ToString() {
            return $"{Updated.ToString("yyyy-MM-dd HH:mm:ss.fff") } {Status} {millisec}ms {Text} ";
        }
    }
}