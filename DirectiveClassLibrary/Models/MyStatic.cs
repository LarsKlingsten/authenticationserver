﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Klingsten.DirectiveClassLibrary.Models {
    public static class MyStatic {

        public static readonly string PASSWORD_FOR_SERVICE = "pass";
     

        public static DateTime Now() {
            return DateTime.UtcNow;
        }

        public static ApiResponse SERVICES_DOES_NOT_EXIST = new ApiResponse(HttpCode.Not_Found, "Service does not exist", null);
        public static ApiResponse USE_DIRECTIVE_INSTEAD_OF_REQUEST = new ApiResponse(HttpCode.Bad_Request, "The Authentication Server is set to use directives (not per request as you tried). Directives means that the authentication is to be done by the the client (by you). Alternatively ask the DS admin to set the server to use UseDirective==false (you should prepare for a 'no'!)", null);
        public static ApiResponse SERVICE_HAS_NO_USERS = new ApiResponse(HttpCode.No_Content, "Service has no users", null);
        public static ApiResponse PASSWORD_REJECTED = new ApiResponse(HttpCode.Unauthorized, "Password Rejected", null);
        public static ApiResponse USER_NOT_FOUND_OR_NO_ACCESS_TO_SERVICE = new ApiResponse(HttpCode.Not_Found, "User not Found or does not have access to service", null);
        public static ApiResponse API_SERVICE_PASSWORD_REJECTED = new ApiResponse(HttpCode.Unauthorized, "Api Password failure", null);
        public static ApiResponse USER_APIKEY_DOES_NOT_EXIST = new ApiResponse(HttpCode.Not_Found, "User not found", null);
        public static ApiResponse USER_NO_ACCESS_TO_SERVICE = new ApiResponse(HttpCode.Unauthorized, "User has not access to service", null);

        public static readonly String SessionUser = "##SessionUser##";
    }
}
