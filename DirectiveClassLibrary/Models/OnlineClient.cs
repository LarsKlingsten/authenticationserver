﻿using System;

namespace Klingsten.DirectiveClassLibrary.Models {
    public class OnlineClient {

        public DateTime FirstRequest { get; set; }
        public DateTime LastRequest { get; set; }
        public int RequestCount { get; set; }
        public int LastRequestLapsedSeconds { get { return (int)(MyStatic.Now() - LastRequest).TotalSeconds; } }
        public string ClientName { get; set; }
        public int ServiceId { get; set; }
        public bool ClientMustUpdateDirectiveNow { get; set; }
        public int ClientsOnlineCount { get; set; }

        public OnlineClient() {
            LastRequest = MyStatic.Now();
            FirstRequest = MyStatic.Now();
            RequestCount = 0;
            ClientMustUpdateDirectiveNow = true;
        }

        public void AddCount() {
            RequestCount++;
        }
    }
}