﻿namespace Klingsten.DirectiveClassLibrary.Models {
     
    public class RequestResponse {
        public bool IsAuthorized { get; set; }

        public int Reset { get; set; }
        public int Count { get; set; }
        public int Limit { get; set; }
        public int Remaining { get; set; }
        public string Message { get; set; }

        public override string ToString() {
            return $"IsAuthorized={IsAuthorized} Limit={Limit} Remaining={Remaining} Reset={Reset}";
        }
    }
}