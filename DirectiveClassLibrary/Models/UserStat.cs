﻿using System;
using System.Threading;
using Klingsten.DirectiveClassLibrary.Services;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class UserStat {
        private int countCurrentPeriod;
        public int countHistoric { get; set; }
        private int countCurrentPeriodDeclined;
        public int countHistoricDeclined { get; set; }
        public DateTime StartTimeUtc { get; set; }
        public DateTime NextReset { get; set; }
        public VmUserService VmUserService { get; set; }
        public int Remaining { get { return VmUserService.Limit - countCurrentPeriod; } }
        public VmDirective VmDirective { get; set; }

        public UserStat(VmUserService vmUserService, VmDirective vmDirective) {
            countCurrentPeriod = 0;
            countHistoric = 0;
            countCurrentPeriodDeclined = 0;
            countHistoricDeclined = 0;
            StartTimeUtc = MyStatic.Now();
            NextReset = MyStatic.Now().AddSeconds(vmUserService.Reset);
            this.VmUserService = vmUserService;
            this.VmDirective = vmDirective;
        }

        public void addCount() {
            Interlocked.Increment(ref countCurrentPeriod);
        }

        public void addCountDeclined() {
            Interlocked.Increment(ref countCurrentPeriodDeclined);
        }

        public int GetCountCurrentPeriod() {
            return countCurrentPeriod;
        }

        public int GetCountTotal() {
            return countCurrentPeriod + countHistoric;
        }

        public int GetCountCurrentPeriodDeclined() {
            return countCurrentPeriodDeclined;
        }

        public int GetCountTotalDeclined() {
            return countCurrentPeriodDeclined + countHistoricDeclined;
        }

        public void ResetCurrentCount() {
            countHistoric += countCurrentPeriod;
            countCurrentPeriod = 0;
            countHistoricDeclined += countCurrentPeriodDeclined;
            countCurrentPeriodDeclined = 0;
            NextReset = SyncNextReset();
        }

        /// summary:
        ///          Sync next reset user time - sync'd via with Authentication service, to ensure that all
        ///          Users are reset at the same time, across all instances.
        ///          
        ///          Example:
        ///          Authentication Server is started at 13:00
        ///          Client Instance 0     is started at 14:02 
        ///          User 'Lars' has his access limit reset every 15 minutes = 900 seconds
        /// 
        ///          at 14:03 'Lars' makes a request and the next reset time must be calculated (=userReset)
        ///      
        ///          long secondsTimeDiff   = (14:03 - 13:00 = 63 minutes * 60 ) = 3780 seconds
        ///          long resetPeriodsCount = 3780 / 900 =  4                              // the decimals are discarded
        ///          DateTime NewResetTime  = 13:00 + 4 * 900 + 900 = 4500 seconds or 75 minutes
        ///                                   (13:00 + 75 minutes = 14:15 = next reset time)  
        ///                                              
        ///          This means that the first period will be reset quicker - but subsequent should be on 
        ///          target. Is further ensure that the each instances always resets 'lars' at exactly 
        ///          at the same time 
        private DateTime SyncNextReset() {
            DateTime serverTimeStartupUtc = VmDirective.ServerTimeStartupUtc;
            int userResetEverySecond = VmUserService.Reset;
            int timeDiffSeconds = (int) (MyStatic.Now() - serverTimeStartupUtc).TotalSeconds;
            int numberOfPeriods = timeDiffSeconds / userResetEverySecond; // integer division (decimals discarded)
            return serverTimeStartupUtc.AddSeconds(numberOfPeriods * userResetEverySecond + userResetEverySecond);
        }

        public int NextResetInSeconds() {
            TimeSpan sp = NextReset.Subtract(MyStatic.Now());
            return (int)sp.TotalSeconds;
        }

        public override string ToString() {
            return $"remaining={Remaining} cur={countCurrentPeriod} ResetSec={NextResetInSeconds()}";
        }
    }
}