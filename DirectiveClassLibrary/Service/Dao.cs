﻿using System.Collections.Generic;
using System.Linq;
using Klingsten.DirectiveClassLibrary.Models;
using System;
using System.Diagnostics;
using DirectiveClassLibrary.EF;

namespace Klingsten.DirectiveClassLibrary.Services {
    public class Dao : IDao {

        private ServiceLog log;
        private Stopwatch sw;
        private static Dao instance;

        private Dao() {
            log = ServiceLog.GetInstance();
            sw = new Stopwatch();
        }

        public static Dao GetInstance() {
            if (instance == null) {
                instance = new Dao();
            }
            return instance;
        }

        public DirectiveDBEntities DirectiveDBEntities() {
            return new DirectiveDBEntities();
        }

        public IList<VmService> GetServices() {
            var services = new List<VmService>();
            using (DirectiveDBEntities db = new DirectiveDBEntities()) {
                var serv = (from s in db.Services select s).ToList();
                foreach (var s in serv) {
                    sw.Restart();
                    List<VmEndPoint> endpoints = new List<VmEndPoint>();
                    foreach (var ep in s.EndPoints) {
                        endpoints.Add(new VmEndPoint {
                            Deployment = ep.Deployment,
                            IsEnabled = ep.IsEnabled,
                            Url = ep.Url,
                            Updated = ep.Updated,
                            Created = ep.Created,
                            Id = ep.Id,
                            ServiceId = ep.Fk_service
                        });

                    }
                    services.Add(new VmService {
                        name = s.Name,
                        id = s.id,
                        apikey = s.ApiKey,
                        updated = s.Updated,
                        created = s.Created,
                        RequestFormat = s.RequestFormat,
                        Limit = s.Limit,
                        Reset = s.Reset,
                        TimeOutMilliSec = s.TimeOutMilliSec,
                        httpPost = s.HttpPost,
                        httpGet = s.HttpGet,
                        httpPut = s.HttpPut,
                        IsEnabled = s.IsEnabled,
                        cacheSeconds = s.CacheSeconds,
                        endPoints = endpoints,
                    });

                    log.AddLog(Status.Info, $"Database update. Service={s.Name} added/updated", sw);
                }
            }
            return services;
        }

        public List<VmAllowIpAddress> GetAllowedIpAddresses() {
            using (DirectiveDBEntities db = new DirectiveDBEntities()) {
                return (from ip in db.AllowIpAddresses
                        select new VmAllowIpAddress {
                            Id = ip.Id,
                            IpAddress = ip.IpAddress,
                            Comment = ip.Comment,
                            Updated = ip.Updated,
                            Created = ip.Created,
                            IsEnabled = ip.IsEnabled
                        }).ToList();
            }
        }

        public IList<VmUser> GetUsers() {
            IList<VmUser> vmUsers = new List<VmUser>();

            using (DirectiveDBEntities db = new DirectiveDBEntities()) {
                var users = (from u in db.Users select u);
                foreach (User user in users) {
                    VmUser vmUser = new VmUser {
                        Name = user.Name,
                        Id = user.id,
                        ApiKey = user.ApiKey,
                        Company = user.Company,
                        Created = user.Created,
                        Enabled = user.IsEnabled,
                        Updated = user.Updated,
                        Origons = user.Origons,

                    };
                    vmUsers.Add(vmUser);
                    foreach (UserService us in user.UserServices) {
                        vmUser.AddUserService(new VmUserService {
                            Id = us.Id,
                            ServiceID = us.Fk_service,
                            UserID = us.Fk_user,
                            ServiceName = us.Service.Name,
                            ApiKey = us.User.ApiKey,
                            Limit = us.Limit,
                            Reset = us.Reset,
                            httpGet = us.HttpGet,
                            httpPost = us.HttpPost,
                            httpPut = us.HttpPut,
                            Updated = us.Updated,
                            Created = us.Created
                        });
                    }
                }
            }
            return vmUsers;
        }

        public VmDirective GetDirective() {
            try {
                using (DirectiveDBEntities db = new DirectiveDBEntities()) {
                    return (from d in db.Directives
                            select new VmDirective {
                                UseDirective = d.UseDirective,
                                Id = d.Id,
                                Updated = d.Updated,
                                ReloadSeconds = d.ReloadSeconds
                            }).FirstOrDefault();
                }
            }
            catch (Exception e) {
                log.AddLog(Status.Panic, "Database Must One single Directive entry only: Please Create One.");
                return null;
            }
        }

        public List<VmAdminUser> GetAdminUsers() {

            using (DirectiveDBEntities db = new DirectiveDBEntities()) {
                var adminUsers = from u in db.AdminUsers
                                 select new VmAdminUser {
                                     Id = u.Id,
                                     Name = u.Name,
                                     Password = u.Password,
                                     IsEnabled = u.IsEnabled,
                                     IsAdmin = u.IsAdmin,
                                     Created = u.Created,
                                     Updated = u.Updated
                                 };

                return adminUsers.ToList();
            }
        }

        public void Reset() {
            throw new NotImplementedException();
        }

        public int GetUserCount(VmService vmService) {
            throw new NotImplementedException();
        }

        IList<VmAdminUser> IDao.GetAdminUsers() {
            throw new NotImplementedException();
        }

        public bool AuthenticateServicePassword(string password) {
            throw new NotImplementedException();
        }

        public VmService GetServiceByName(string serviceName) {
            throw new NotImplementedException();
        }

        public VmService GetService(int ServiceId) {
            throw new NotImplementedException();
        }

        public VmUser GetUserById(int id) {
            throw new NotImplementedException();
        }

        public VmUserService GetUserService(int id) {
            throw new NotImplementedException();
        }

        public VmEndPoint GetEndPoint(int id) {
            throw new NotImplementedException();
        }
    }
}