﻿using System.Collections.Generic;
using Klingsten.DirectiveClassLibrary.Models;
using System.Linq;
using DirectiveClassLibrary.EF;

namespace Klingsten.DirectiveClassLibrary.Services {

    public class DaoService : IDao {

        private static DaoService instance;
        private VmDirective _directive;
        private IList<VmService> _services;
        private IList<VmUser> _users;
        private List<VmAllowIpAddress> _allowedIpAddresses;
        private Dao dao;

        public DirectiveDBEntities GetDirectiveDBEntities() {
            return dao.DirectiveDBEntities();
        }

        private DaoService() {
            dao = Dao.GetInstance();
        }

        public static DaoService GetInstance() {
            if (instance == null) {
                instance = new DaoService();
            }
            return instance;
        }

        public void Reset() {
            _directive = null;
            _services = null;
            _users = null;
            _allowedIpAddresses = null;
            UserStatService.GetInstance().Reset();
            OnlineClientService.GetInstance().ForceDirectiveUpdateToAllClients();
        }

        public IList<VmService> GetServices() {
            if (_services == null) {
                _services = dao.GetServices();
                foreach (VmService vmService in GetServices()) {
                    foreach (VmUser vmUser in GetUsers()) {
                        if (vmUser.usersServices.Any(s => s.ServiceID == vmService.id)) {
                            vmService.addVmUser(vmUser);
                        }
                    }
                }
            }
            return _services;
        }

        public VmService GetService(int ServiceId) {
            return GetServices().FirstOrDefault(s => s.id == ServiceId);
        }

        public IList<VmAdminUser> GetAdminUsers() {
            return dao.GetAdminUsers();
        }

        public VmService GetServiceByName(string serviceName) {
            return GetServices().FirstOrDefault(s => s.name.Equals(serviceName));
        }

        public IList<VmUser> GetUsers() {
            if (_users == null) {
                _users = dao.GetUsers();
            }
            return _users;
        }

        public int GetUserCount(VmService vmService) {
            return GetUsers().Count();
        }

        public VmUser GetUser(string apikey) {
            return GetUsers().FirstOrDefault(s => s.ApiKey == apikey);
        }

        public VmUser GetUserById(int id) {
            return GetUsers().FirstOrDefault(s => s.Id == id);
        }

        public VmDirective GetDirective() {
            if (_directive == null) {
                _directive = dao.GetDirective();
                _services = GetServices();
            }
            return _directive;
        }

        public bool AuthenticateServicePassword(string password) {
            if (password.Equals(MyStatic.PASSWORD_FOR_SERVICE)) {
                return true;
            }
            return false;
        }

        public VmEndPoint GetEndPoint(int id) {
            foreach (VmService service in GetServices()) {
                foreach (var ep in service.endPoints) {
                    if (ep.Id == id) {
                        return ep;
                    }
                }
            }
            return null;
        }

        public VmUserService GetUserService(int id) {
            foreach (VmUser user in GetUsers()) {
                foreach (var us in user.usersServices) {
                    if (us.Id == id) {
                        return us;
                    }
                }
            }
            return null;
        }


        public VmUserService GetVmUserService(VmUser vmUser, VmService vmService) {
            return vmUser.usersServices.Where(us => us.ServiceID == vmService.id).FirstOrDefault();
        }

        public List<VmAllowIpAddress> GetAllowedIpAddresses() {
            if (_allowedIpAddresses == null) {
                _allowedIpAddresses = dao.GetAllowedIpAddresses();
            }
            return _allowedIpAddresses;
        }

        public bool IsIpAddressAllowed(string ipAddress) {
            if (GetAllowedIpAddresses().Exists(s => s.IpAddress.Equals(ipAddress) && s.IsEnabled)) {
                return true;
            }
            return false;
        }
    }
}