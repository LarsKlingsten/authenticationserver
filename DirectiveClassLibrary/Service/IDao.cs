﻿using Klingsten.DirectiveClassLibrary.Models;
using System.Collections.Generic;

namespace Klingsten.DirectiveClassLibrary.Services {

    public interface IDao {
        void Reset();

        VmDirective GetDirective();

        IList<VmService> GetServices();
        int GetUserCount(VmService vmService);
        VmService GetServiceByName(string serviceName);
        VmService GetService(int ServiceId);

        IList<VmUser> GetUsers();
        VmUser GetUserById(int id);

        IList<VmAdminUser> GetAdminUsers();

        bool AuthenticateServicePassword(string password);

        VmUserService GetUserService(int id);

        VmEndPoint GetEndPoint(int id);

        List<VmAllowIpAddress> GetAllowedIpAddresses();
    }
}
