﻿using System.Collections.Generic;
using System.Diagnostics;
using Klingsten.DirectiveClassLibrary.Models;

namespace Klingsten.DirectiveClassLibrary.Services {
    
    public class ServiceLog {
        private List<LogEntry> _MyLog;
        private static ServiceLog _instance;

        private ServiceLog() {
            _MyLog = new List<LogEntry>(1000);
        }

        public static ServiceLog GetInstance() {
            if (_instance == null) {
                _instance = new ServiceLog();
            }
            return _instance;
        }

        public void AddLog(Status status, string entry, Stopwatch sw = null) {
            LogEntry logEntry = new LogEntry(status, entry, sw);
            Debug.WriteLine(logEntry);
            _MyLog.Add(logEntry);
            if (_MyLog.Count > 999) {
                _MyLog.RemoveRange(0, 100);
            }
        }

        public List<LogEntry> GetLogs() {
            return _MyLog;
        }

        public List<string> GetLogsAsString() {
            var logs = new List<string>();
            int count = _MyLog.Count - 1;
            for (int i = count; i > -1; i--) {
                logs.Add(_MyLog[i].ToString());
            }
            return logs;
        }
    }
}