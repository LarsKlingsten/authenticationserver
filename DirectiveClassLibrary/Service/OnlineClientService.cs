﻿using Klingsten.DirectiveClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace Klingsten.DirectiveClassLibrary.Services {
    public class OnlineClientService {

        private List<OnlineClient> onlineClients;
        private static OnlineClientService instance;
        private IDao daoService;

        private OnlineClientService() {
            onlineClients = new List<OnlineClient>();
            daoService = DaoService.GetInstance();
        }

        public static OnlineClientService GetInstance() {
            if (instance == null) {
                instance = new OnlineClientService();
            }
            return instance;
        }

        public void ForceDirectiveUpdateToAllClients() {
            foreach (var o in onlineClients) {
                o.ClientMustUpdateDirectiveNow = true;
            }
        }

        public OnlineClient GetOnlineClient(string clientName, int serviceId) {
            OnlineClient onlineClient = onlineClients.FirstOrDefault(s => s.ClientName == clientName && s.ServiceId == serviceId);
            if (onlineClient == null) {
                onlineClient = new OnlineClient { ClientName = clientName, ServiceId = serviceId };
                onlineClients.Add(onlineClient);
            }
            return onlineClient;
        }

        public void UpdateOnlineClientAccessTime(OnlineClient onlineClient, VmService vmService) {
            onlineClient.AddCount();
            int count = GetCurrentOnlineClients(vmService.id).Count;
            if (count != onlineClient.ClientsOnlineCount) {
                onlineClient.ClientsOnlineCount = count;
                onlineClient.ClientMustUpdateDirectiveNow = true;
            }
            onlineClient.LastRequest = MyStatic.Now();
        }

        public List<OnlineClient> GetAllOnlineClients() {
            return onlineClients;
        }

        public List<OnlineClient> GetCurrentOnlineClients(int serviceId) {
            const int MARGIN = 10; // allows delay in seconds in client upload
            return onlineClients.Where(s => s.ServiceId == serviceId && s.LastRequest > MyStatic.Now()
                   .AddSeconds(-daoService.GetDirective()
                   .ReloadSeconds - MARGIN)).ToList();
        }

        // Summary:
        //          Used to reduce the number of request for each user per client-server when 
        //          multiple client  (api servers) are deployed 
        //
        //          the formula yields following results (3 examples)
        //
        //          Example    	  Number Of Clients  Request for Client   Total possible
        //          Access Level  (API servers)		 Per Server*            Request
        //             100          	1	              100    	          100 (1x100)
        //             100         	    2	               55	              110 (2x 55)
        //             100           	3	               40	              120 (3x 40)
        //
        //          as this allow a margin of request for each server 
        //
        //          * = returned result
        // 
        public int CalcAccessFactor(int serviceId) {
            int clientsCount = GetCurrentOnlineClients(serviceId).Count;
            decimal fac = 0.9m + decimal.Divide(clientsCount, 10);
            return (int)(1m / clientsCount * fac * 100);
        }
    }
}