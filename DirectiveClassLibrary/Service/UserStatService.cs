﻿using System.Collections.Generic;
using Klingsten.DirectiveClassLibrary.Models;

namespace Klingsten.DirectiveClassLibrary.Services {

    public class UserStatService {

        private Dictionary<string, UserStat> userStatsDict;
        private static UserStatService instance;

        private UserStatService() {
            userStatsDict = new Dictionary<string, UserStat>();
        }

        public static UserStatService GetInstance() {
            if (instance == null) {
                instance = new UserStatService();
            }
            return instance;
        }

        public void Reset() {
            userStatsDict = new Dictionary<string, UserStat>();
        }

        public UserStat GetUserStatByKey(string key) {
           
            UserStat userStat;
            userStatsDict.TryGetValue(key, out userStat);
            return userStat;
        }

        public string GetKey(string ApiKey, string serviceName) {
            return $"{ApiKey}##{serviceName}";
        }

        public UserStat CreateUserStat(string apiKey, string serviceName, VmUserService vmUserService , VmDirective vmDirective ) {
            string key = GetKey(apiKey, serviceName);
            UserStat userStat = GetUserStatByKey(key);
            if (userStat == null) {
                userStat = new UserStat(vmUserService, vmDirective);
                userStatsDict.Add(key , userStat);
            }
            return userStat;
        }

        public bool IsWithinLimits(UserStat userStat) {
            if (userStat.GetCountCurrentPeriod() >= userStat.VmUserService.Limit) { //   User count within period exceeded
                if (ShouldResetUserCount(userStat)) {
                    userStat.ResetCurrentCount();
                    return true;
                }
                return false;
            }
            return true;
        }

        private bool ShouldResetUserCount(UserStat userStat) {
            if (userStat.NextResetInSeconds() < 0) {
                return true;
            }
            return false;
        }
    }
}