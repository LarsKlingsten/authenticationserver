﻿using DirectiveClassLibrary.EF;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class VmAdminUser {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Name length must be less than  must be 2 - 50 characters long characters", MinimumLength = 2)]
        public string Name { get; set; }

        [DisplayName("Login Password")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Password length must be  8 - 50 characters long")]
        [Required(ErrorMessage = "Please enter your password")]
        public string Password { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsAdmin { get; set; }

        public AdminUser ToAdminUser() {
            AdminUser adminUser = new AdminUser {
                Name = Name,
                Password = Password,
                IsAdmin = IsAdmin,
                IsEnabled = IsEnabled,
                Updated = Updated,
                Created = Created
            };
            return adminUser;
        }

        public static VmAdminUser ToVmAdminUser(AdminUser au) {
            VmAdminUser vmAdminUser = new VmAdminUser {
                Name = au.Name,
                Password = au.Password,
                IsAdmin = au.IsAdmin,
                IsEnabled = au.IsEnabled,
                Updated = au.Updated,
                Created = au.Created,
            };
            return vmAdminUser;
        }
    }
}