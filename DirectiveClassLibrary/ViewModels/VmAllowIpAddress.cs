﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class VmAllowIpAddress {

        public int Id { get; set; }

        [RegularExpression(@"^[0-9:.]*$", ErrorMessage = "Please use '0-9:.' only")]
        [StringLength(16, ErrorMessage = "Length 2 - 16 characters only", MinimumLength = 2)]
        public string IpAddress { get; set; }

        [StringLength(50, ErrorMessage = "Length 2 - 50 characters only", MinimumLength = 2)]
        public string Comment { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }

        public bool IsEnabled { get; set; }
    }
}