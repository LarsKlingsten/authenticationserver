﻿using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class VmDirective {

        private static readonly DateTime SERVERTIME_STARTUP_UTC = DateTime.UtcNow;

        public int Id { get; set; }
     
        public int ReloadSeconds { get; set; }
        public bool UseDirective { get; set; }

        public DateTime ServerTimeStartupUtc { get; set; }
        public DateTime ServerTimeUtcNow { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }
        public List<VmService> Services { get; set; }

        public VmDirective() {
            Services = new List<VmService>();
            ServerTimeStartupUtc = SERVERTIME_STARTUP_UTC;
            ServerTimeUtcNow = MyStatic.Now();
        }

        public VmDirective CopyOfDirective() {
            return new VmDirective {
                ReloadSeconds = ReloadSeconds,
                UseDirective = UseDirective,
                Id = Id,
            };
        }
    }
}