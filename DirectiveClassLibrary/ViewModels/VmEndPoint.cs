﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class VmEndPoint {
        public int Id { get; set; }

        [StringLength(10, MinimumLength = 2, ErrorMessage = "Must be  2-10 characters long")]
        [Required(ErrorMessage = "Please enter")]
        public string Deployment { get; set; }

        [RegularExpression(@"^[https://][a-zA-Z0-9/:.-]*$", ErrorMessage = "Must start with 'https://' and 'a-zA-Z0-9/:.-' chars only")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "Must be  5-100 characters long")]
        [Required(ErrorMessage = "Please enter URL")]
        public string Url { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }
        public bool IsEnabled { get; set; }
        public int ServiceId { get; set; }
    }
}