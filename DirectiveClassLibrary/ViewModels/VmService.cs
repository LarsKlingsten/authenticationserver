﻿using DirectiveClassLibrary.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class VmService {
        public int id { get; set; }

        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9.-]*$", ErrorMessage = "Must start with A-z and contain only be 'A-z0-9-._' chars only")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Must be  3-25 characters long")]
        [Required(ErrorMessage = "Please enter Service Name")]
        public string name { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime updated { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime created { get; set; }

        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9.-]*$", ErrorMessage = "Must start with A-z and contain only be 'A-z0-9-._' chars only")]
        [StringLength(25, MinimumLength = 8, ErrorMessage = "Must be  8-25 characters long")]
        [Required(ErrorMessage = "Please enter ApiKey (password)")]
        public string apikey { get; set; }

        [DisplayName("Limit")]
        public int Limit { get; set; }

        [DisplayName("Reset")] // reset every
        public int Reset { get; set; }

        public bool httpGet { get; set; }
        public bool httpPut { get; set; }
        public bool httpPost { get; set; }

        [DisplayName("Timeout")]
        public int TimeOutMilliSec { get; set; }

        [DisplayName("Cache")]
        public int cacheSeconds { get; set; }

        [DisplayName("Format")]
        [Required(ErrorMessage = "Please enter Api Key")]
        public string RequestFormat { get; set; }

        public bool IsEnabled { get; set; }
        public List<VmEndPoint> endPoints { get; set; }
        public List<VmUser> users { get; set; }
        public List<VmUserService> userServices { get; set; }

        public VmService() {
            users = new List<VmUser>();
            endPoints = new List<VmEndPoint>();
            userServices = new List<VmUserService>();
        }

        public void addVmUser(VmUser vmUser) {
            users.Add(vmUser);
        }

        public void addEndPoint(VmEndPoint vmEndPoint) {
            endPoints.Add(vmEndPoint);
        }
        public void addUserService(VmUserService vmUserService) {
            userServices.Add(vmUserService);
        }

        public Service ToService() {
            Service service = new Service {
                id = id,
                Name = name,
                ApiKey = apikey,
                HttpGet = httpGet,
                HttpPost = httpPost,
                HttpPut = httpPut,
                IsEnabled = IsEnabled,
                CacheSeconds = cacheSeconds,
                RequestFormat = RequestFormat,
                Limit = Limit,
                Reset = Reset,
                TimeOutMilliSec = TimeOutMilliSec,
                Updated = updated,
                Created = created
            };
            return service;
        }
    }
}