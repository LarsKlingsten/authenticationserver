﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DirectiveClassLibrary.EF;

namespace Klingsten.DirectiveClassLibrary.Models {

    public class VmUser {
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Must be  3-50 characters long")]
        [Required(ErrorMessage = "Please enter user name")]
        public string Name { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Must be  3-50 characters long")]
        [Required(ErrorMessage = "Please enter Company Name")]
        public string Company { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }

        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9.-]*$", ErrorMessage = "Must start with A-z and contain only be 'A-z0-9-._' chars only")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Must be  3-50 characters long")]
        [Required(ErrorMessage = "A Unique Api Key is required")]
        public string ApiKey { get; set; }

        public bool Enabled { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "Must be  1-50 characters long")]
        [Required(ErrorMessage = "Must be a regular expression or * (*=all)")]
        public string Origons { get; set; }

        public IList<VmUserService> usersServices { get; set; }

        public VmUser() {
            usersServices = new List<VmUserService>();
        }

        public void AddUserService(VmUserService us) {
            usersServices.Add(us);
        }

        public VmUser GetPartialCopyOfVmUser() {
            return new VmUser {
                Id = Id,
                Name = Name,
                ApiKey = ApiKey,
                Origons = Origons
            };
        }

        public User ToUser() {
            return new User {
                ApiKey = ApiKey,
                Company = Company,
                Created = Created,
                Updated = Updated,
                IsEnabled = Enabled,
                id = Id,
                Name = Name,
                Origons = Origons
            };
        }
    }
}