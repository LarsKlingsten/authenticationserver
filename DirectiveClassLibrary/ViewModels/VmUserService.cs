﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;

namespace Klingsten.DirectiveClassLibrary.Models {

    // This the table that holds both userId and ServiceId

    public class VmUserService {

        public int Id { get; set; }
        public int UserID { get; set; }
        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string ApiKey { get; set; }

        public int Limit { get; set; }
        public int Reset { get; set; }

        public bool httpGet { get; set; }
        public bool httpPost { get; set; }
        public bool httpPut { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }

        public bool HasHttpMethodRights(HttpMethod httpMethod) {
            if (httpGet && httpMethod == HttpMethod.Get) {
                return true;
            }
            else if (httpPut && httpMethod == HttpMethod.Put) {
                return true;
            }
            else if (httpPost && httpMethod == HttpMethod.Post) {
                return true;
            }
            return false;
        }
    }
}
