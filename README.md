Scalable Support Authentication Solution (for Dansk Supermarked Group A/S)
----------------------------------------

COMPUTER SCIENCE
FINAL PROJECT
(DATAMATIKER AFSLUTTENDE PROJEKT)

Title:               Scalable Support Authentication Solution

Author:              Lars Klingsten
                     email: lars@klingsten.net

Class:               13T, 5th Semester, Aarhus Erhvervsakademi, Datamatiker

Company:             Dansk Supermarked Group A/S (“DS”)
                     Bjødstrupvej 18, DK-8270 Højbjerg
                     Contact: Mr. Thomas Michael Nielsen
                     https://dansksupermarked.com

Supervisor:           Mr. Jørn Martin Hajek, Lektor, Aarhus Erhvervsakademi. Email: jhaj@eaaa.dk

Abstract (Synopsis):

For pre-approved partners and for Dansk Supermarked A/S (“DS”) own public websites and applications,
DS has made a select set internal data publicly available. This data, including pricing, product information,
discounts and newsletters, is accessible via a number of micro-services, internally known as DS APIs.
This project discusses DS current authentication and authorization setup, in particular current
implementation issues that hampers efficient user administration, lacks scalability in Azure and service
downtime.

This project implements a new ASP.NET Authentication server (a prototype that runs in Azure, plus a client)
that fits into DS current set of APIs, that could replace DS’s existing authentication and authorization setup.
The new Authentication Server has two operating modes: “Request Mode” which focuses on simple
refactoring process of DS existing code base and a “Directives Mode” that focus raw response
performance, but requires more complex logic at the existing clients, and a more involved refactoring
process.

The new Authentication Server allows resilient auto scalability at Azure, performance and efficient service
administration.